/mob/living/advanced/npc/unique/hostage/
	ai = /ai/advanced/hostage
	health_base = 200 //Extra health so the escort mission isn't fucking hell.
	var/hostage = TRUE
	queue_delete_on_death = FALSE

/mob/living/advanced/npc/unique/hostage/Initialize()
	. = ..()
	src.add_organ(/obj/item/organ/internal/implant/hand/left/iff/nanotrasen)
	src.add_organ(/obj/item/organ/internal/implant/head/loyalty/nanotrasen)
	return .

/mob/living/advanced/npc/unique/hostage/assistant
	name = "Urist McRobust"
	sex = MALE
	gender = MALE
	dialogue_id = /dialogue/npc/hostage/assistant

/mob/living/advanced/npc/unique/hostage/assistant/Initialize()
	. = ..()
	update_all_blends()
	equip_loadout(/loadout/nt/survivor)
	return .


/mob/living/advanced/npc/unique/hostage/clown
	name = "Charles the Clown"
	sex = MALE
	gender = MALE
	dialogue_id = /dialogue/npc/hostage/assistant

/mob/living/advanced/npc/unique/hostage/clown/Initialize()
	. = ..()
	update_all_blends()
	equip_loadout(/loadout/clown)
	return .


/mob/living/advanced/npc/unique/hostage/scientist
	name = "Michael Stone"
	sex = MALE
	gender = MALE
	dialogue_id = /dialogue/npc/hostage/assistant

/mob/living/advanced/npc/unique/hostage/scientist/Initialize()
	. = ..()
	update_all_blends()
	equip_loadout(/loadout/scientist)
	return .

/mob/living/advanced/npc/unique/hostage/halo/civilian
	name = "Civilian"
	enable_AI = TRUE
	ai = /ai/advanced/hostage
	dialogue_id = /dialogue/npc/hostage/
	var/loadout_to_use = /loadout/scientist

/mob/living/advanced/npc/unique/hostage/halo/civilian/Initialize()

	. = ..()

	var/species/S = SPECIES(species)

	sex = pick(MALE,FEMALE)
	gender = sex

	change_organ_visual("skin", desired_color = pick("#E0BCAA","#BC9E8F","#967F73","#7A675E"))

	var/hair_color = random_color()
	change_organ_visual("hair_head", desired_color = hair_color, desired_icon_state = pick(S.all_hair_head))
	if(sex == MALE && prob(25))
		change_organ_visual("hair_face", desired_color = hair_color, desired_icon_state = pick(S.all_hair_face))

	src.add_organ(/obj/item/organ/internal/implant/hand/left/iff/nanotrasen)
	src.add_organ(/obj/item/organ/internal/implant/head/loyalty/nanotrasen)

	update_all_blends()

	equip_loadout(loadout_to_use)

	return .