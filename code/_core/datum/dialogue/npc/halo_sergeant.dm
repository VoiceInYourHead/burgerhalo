/dialogue/npc/halosl/

/dialogue/npc/halosl/get_dialogue_options(var/mob/living/advanced/player/nt/halo/P,var/list/known_options)

	var/mob/living/L = P.dialogue_target

	. = list()

	if(L.following) //If we're following someone
		if(L.following == P) //If we're following the person we're talking to.
			.["hello"] = list(
				"What do you need, marine?",
				"*stop following me"
			)
			.["*stop following me"] = list(
				"Got it, others - fall back to the last position!"
			)
		else //If we're following someone else.
			.["hello"] = list(
				"What do you need, marine?",
				"*follow me"
			)
			.["*follow me"] = list(
				"Wait until i will stop speaking to [L.following.real_name], marine!"
			)
	else //We're not following someone.
		.["hello"] = list(
			"What's the name of your the group, marine?",
			"Foxtrot Company"
		)
		.["Foxtrot Company"] = list(
			"So you has been sent here to rescue us and destroy the facility? Take device in my cabinet - it will help your team alot.",
			"*follow me"
		)
		if(length(P.followers) < 4) //They have less than 4 followers
			.["*follow me"] = list(
				"Let's go!"
			)
		else
			.["*follow me"] = list(
				"You have enough of my people already! Just go!"
			)

	return .


/dialogue/npc/halosl/set_topic(var/mob/living/advanced/player/nt/halo/P,var/topic)

	. = ..()

	if(!is_living(P.dialogue_target))
		return .

	var/mob/living/L = P.dialogue_target

	if(!L.ai)
		return .

	switch(topic)
		if("*stop following me")
			if(L in P.followers)
				L.ai.set_move_objective(null)
				L.following = null
				P.followers -= L
		if("*follow me")
			if(length(P.followers) < 4 && !L.following)
				P.followers += L
				L.ai.set_move_objective(P,TRUE)
				L.following = P
	return .